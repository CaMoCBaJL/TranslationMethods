﻿using GrammarParserLib;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            var controller = new GrammarAlgorithms();

            controller.DATDP(DAL.GetDataFromFile(args[0]), DAL.GetAxiom(args[0]));
        }
    }
}
