﻿namespace Entities
{
    public class AlgorithmConfiguration
    {
        public AlgCondition Condition { get; private set; }
        public uint InputIntPtr { get; private set; }
        public string L1Content { get; private set; }
        public string L2Content { get; private set; }

        public AlgorithmConfiguration(string axiom)
        {
            Condition = AlgCondition.q;
            InputIntPtr = 1;
            L1Content = string.Empty;
            L2Content = axiom + "$";
        }

        public void SetL1Content(string newContent) => L1Content = newContent;
        public void SetL2Content(string newContent) => L2Content = newContent;
        public void AddSymbolToL1(string symbol) => L1Content = L1Content + symbol;
        public void AddSymbolToL2(string symbol) => L2Content = L2Content + symbol;
        public void RemoveSymbolFromL1(string symbol)
        {
            if (L1Content.Contains(symbol))
                L1Content = L1Content.Remove(L1Content.LastIndexOf(symbol), symbol.Length);
        }
        public void RemoveSymbolFromL2(string symbol)
        {
            if (L2Content.Contains(symbol))
                L2Content = L2Content.Remove(L2Content.LastIndexOf(symbol), symbol.Length);
        }
    }
}
