﻿namespace Entities
{
    public class GrammarRule
    {
        public string NonTerminal { get; init; }
        public string Output { get; private set; }
        
        public GrammarRule(string nonTerminal, string output)
        {
            NonTerminal = nonTerminal;
            Output = output;
        }

        public void ChangeOuptut(string newOutput) => Output = newOutput;

        public override bool Equals(object obj)
        {
            GrammarRule rule = obj as GrammarRule;
            return rule.NonTerminal == NonTerminal && rule.Output == Output;
        }

        public override int GetHashCode()
        => Output.GetHashCode() ^ NonTerminal.GetHashCode();
        
    }
}
