﻿using System.Collections.Generic;
using System;
using System.Linq;

namespace Entities
{
    public class Grammar
    {
        List<GrammarRule> _rules;
        List<string> _terminals;
        List<string> _nonTerminals;

        public string Axiom { get; init; }
        public List<string> Terminals
        {
            get => new List<string>(_terminals);
            init
            {
                if (value == null)
                    throw new ArgumentNullException();

                _terminals = value;
            }
        }
        public List<string> NonTerminals
        {
            get => new List<string>(_nonTerminals);
            init
            {
                if (value == null)
                    throw new ArgumentNullException();

                _nonTerminals = value;
            }
        }
        public List<GrammarRule> Rules
        {
            get => new List<GrammarRule>(_rules);
            init
            {
                if (value == null)
                    throw new ArgumentNullException();

                _rules = value;
            }
        }

        public void AddRule(GrammarRule rule) => _rules.Add(rule);
        public void AddNonTerminal(string nonTerminal) => _nonTerminals.Add(nonTerminal);
        public void AddTerminal(string terminal) => _terminals.Add(terminal);
        public void RemoveRules(IEnumerable<GrammarRule> rules) => _rules.RemoveAll(rule => rules.Contains(rule));
        public void RemoveTerminals(IEnumerable<string> terminals) => _terminals.RemoveAll(symbol => terminals.Contains(symbol));
        public void RemoveNonTerminals(IEnumerable<string> nonTerminals) => _nonTerminals.RemoveAll(symbol => nonTerminals.Contains(symbol));
    }
}
