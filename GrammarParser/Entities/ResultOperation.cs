﻿using System;

namespace Entities
{
    [Flags]
    public enum ResultOperation
    {
        LanguageIsEmpty,
        AlgSuccess,
    }
}
