﻿using GrammarParserLib;
using System;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            var controller = new GrammarAlgorithms();

            controller.RemoveUselessSymbols(DAL.GetDataFromFile(args[0]), DAL.GetAxiom(args[0]));
        }
    }
}
