﻿using System;
using System.Linq;
using System.IO;

namespace GrammarParserLib
{
    public class DAL
    {
        public static string GetDataFromFile(string fileName)
        => string.Join("\r\n",
            File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName))
            .ToList()
            .FindAll(str => str.Contains("->")));

        public static string GetAxiom(string fileName)
        => File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName))[0];
    }
}
