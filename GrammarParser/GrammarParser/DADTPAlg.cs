﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace GrammarParserLib
{
    internal class DADTPAlg
    { 
        public ResultOperation StartAlg(Grammar grammar, out List<AlgorithmConfiguration> algLog)
        {
            var startConfiguration = new AlgorithmConfiguration(grammar.Axiom);
            algLog = new List<AlgorithmConfiguration>();

            while (true)
            {
                if (algLog.Last().L2Content == "$")
                    break;
            }    

            return ResultOperation.AlgSuccess;
        }
    }
}
