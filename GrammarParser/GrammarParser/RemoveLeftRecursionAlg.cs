﻿using System.Collections.Generic;
using System.Linq;
using Entities;

namespace GrammarParserLib
{
    internal class RemoveLeftRecursionAlg
    {
        public void RemoveLeftRecursion(Grammar grammar)
        {
            var oldNonTerminals = grammar.NonTerminals;
            var newNonTerminals = new List<string>();
            var newGrammarRules = new List<GrammarRule>();
            int i = 0;
            while (i < grammar.NonTerminals.Count)
            {
                if (oldNonTerminals.Contains(grammar.NonTerminals[i]) && grammar.Rules
                    .Any(rule => rule.NonTerminal == grammar.NonTerminals[i] && rule.Output
                    .Contains(grammar.NonTerminals[i]))
                    )
                {
                    var newNonTerm = grammar.NonTerminals[i] + '\'';
                    grammar.AddNonTerminal(newNonTerm);

                    var currentNonTerminalRules = grammar.Rules
                    .FindAll(rule => rule.NonTerminal == grammar.NonTerminals[i]);

                    List<GrammarRule> newRules = CalculateNewRules(grammar, i, newNonTerm, currentNonTerminalRules);

                    grammar.RemoveRules(currentNonTerminalRules);
                    newRules.ForEach(rule => grammar.AddRule(rule));
                    i++;
                }
                else
                    i++;
            }
        }

        private static List<GrammarRule> CalculateNewRules(Grammar grammar, int i, string newNonTerm, List<GrammarRule> currentNonTerminalRules)
        {
            var newRules = new List<GrammarRule>();
            foreach (var rule in currentNonTerminalRules)
            {
                if (rule.Output.IndexOf(grammar.NonTerminals[i]) == 0)
                {
                    newRules.Add(new GrammarRule(grammar.NonTerminals[i],
                        rule.Output.Replace(grammar.NonTerminals[i], string.Empty) + newNonTerm));
                    newRules.Add(new GrammarRule(newNonTerm,
                        rule.Output.Replace(grammar.NonTerminals[i], string.Empty) + newNonTerm));
                    newRules.Add(new GrammarRule(newNonTerm,
                        rule.Output.Replace(grammar.NonTerminals[i], string.Empty)));
                }
                else
                {
                    newRules.Add(rule);
                }
            }

            return newRules;
        }
    }
}
