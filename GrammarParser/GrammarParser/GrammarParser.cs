﻿using System.Collections.Generic;
using System;
using System.Linq;
using Entities;

namespace GrammarParserLib
{
    public class GrammarParser
    {
        public Grammar ParseGrammar(string data, string axiom)
        {
            if (!data.Contains(axiom))
                throw new ArgumentException();
            List<string> nonTerminals = GetNonTerminals(data);
            List<string> terminals = GetTerminals(data, nonTerminals);
            return new Grammar() { NonTerminals = nonTerminals, Terminals = terminals, Rules = GetRules(data), Axiom = axiom };
        }

        private List<GrammarRule> GetRules(string data)
        {
            HashSet<GrammarRule> result = new HashSet<GrammarRule>();

            foreach (var item in data.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries))
            {
                var sourceData = item.Split("->");
                var nonTerminal = sourceData[0].Trim();
                var rules = sourceData[1].Split('|', StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < rules.Length; i++)
                    result.Add(new GrammarRule(nonTerminal, rules[i].Trim()));
            }

            return result.ToList();
        }

        private List<string> GetTerminals(string data, List<string> nonTerminals)
        {
            var resultData = data.Replace("->", string.Empty).ToCharArray().ToList();
            resultData.RemoveAll(c => nonTerminals.Contains(c.ToString()) || c == '|' || char.IsWhiteSpace(c));
            return resultData.Distinct().Select(c => c.ToString()).ToList();
        }

        private List<string> GetNonTerminals(string data)
        {
            HashSet<string> result = new HashSet<string>();
            foreach (var item in data.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries))
                result.Add(item.Split("->")[0].Trim());

            return result.ToList();
        }
    }
}
