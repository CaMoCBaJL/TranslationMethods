﻿using System.Collections.Generic;
using Entities;

namespace GrammarParserLib
{
    public class GrammarAlgorithms
    {
        public ResultOperation RemoveUselessSymbols(string grammarData, string axiom)
        {
            Grammar grammar = new GrammarParser().ParseGrammar(grammarData, axiom);
            return RemoveBarrenSymbols(grammar) | RemoveUnreachableSymbols(grammar);
        }

        public ResultOperation RemoveBarrenSymbols(Grammar grammar)
        => new RemoveBarrenSymbolsAlg().RemoveBarrenSymbols(grammar);
        
        public ResultOperation RemoveUnreachableSymbols(Grammar grammar)
        => new RemoveUnreachableSymbolsAlg().StartAlgorithm(grammar);

        internal static bool CompareLists<T>(List<T> list1, List<T> list2)
        {
            if (list1.Count == list2.Count)
            {
                for (int i = 0; i < list1.Count; i++)
                {
                    if (!list1[i].Equals(list2[i]))
                        return false;
                }
                return true;
            }
            return false;
        }

        public ResultOperation DATDP(string grammarData, string axiom)
        {
            Grammar grammar = new GrammarParser().ParseGrammar(grammarData, axiom);
            //new RemoveCyclesAlg().RemoveCycles(grammar);
            new RemoveLeftRecursionAlg().RemoveLeftRecursion(grammar);
            DATDP(grammar);
            return ResultOperation.AlgSuccess;
        }

        private void DATDP(Grammar grammar)
        {
            throw new System.NotImplementedException();
        }

    }
}
