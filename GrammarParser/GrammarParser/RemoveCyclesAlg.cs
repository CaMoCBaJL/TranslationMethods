﻿using System.Collections.Generic;
using System.Linq;
using Entities;

namespace GrammarParserLib
{
    internal class RemoveCyclesAlg
    {
        public void RemoveCycles(Grammar grammar)
        {
            var newRules = CalculateNewRules(grammar);
            grammar.RemoveRules(grammar.Rules);
            newRules.ForEach(rule => grammar.AddRule(rule));
        }

        private List<GrammarRule> CalculateNewRules(Grammar grammar)
        {
            var N_As = CalculateN_As(grammar);
            List<GrammarRule> result = new List<GrammarRule>();
            foreach (var N_A in N_As)
            {
                var foundRules = new List<GrammarRule>();
                N_A
               .ForEach(nonTerminal => foundRules
               .AddRange(grammar.Rules
               .FindAll(rule => !grammar.NonTerminals
               .Any(symbol => rule.Output == symbol) && rule.NonTerminal == nonTerminal && !result
               .Contains(rule))));

                N_A.ForEach(nonTerminal => foundRules
                .ForEach(rule => result
                .Add(new GrammarRule(nonTerminal, rule.Output))));
            }
            return result;
        }

        private List<List<string>> CalculateN_As(Grammar grammar)
        {
            List<List<string>> N_ACollection = new List<List<string>>();
            List<string> N_A = new List<string>();
            List<string> newNonterminalsToAdd = new List<string>();
            foreach (var nonTerminal in grammar.NonTerminals)
            {
                N_A.Add(nonTerminal);
                do
                {
                    newNonterminalsToAdd.Clear();
                    newNonterminalsToAdd = grammar.Rules
                    .FindAll(rule => N_A
                    .Any(foundNonTerminal => !N_A.Contains(rule.NonTerminal) && rule.Output == foundNonTerminal))
                    .Select(rule => rule.NonTerminal)
                    .ToList();

                    N_A.AddRange(newNonterminalsToAdd);
                } while (newNonterminalsToAdd.Count != 0);
                N_ACollection.Add(new List<string>(N_A));
                N_A.Clear();
            }
            return N_ACollection;
        }
    }
}
