﻿using System.Collections.Generic;
using System.Linq;
using Entities;

namespace GrammarParserLib
{
    public static class GrammarRuleExtentionMethod
    {
        public static bool RuleOuputConsistsOfFoundNonterminalsAndTerminals(this GrammarRule rule, IEnumerable<string> otherNonTerminals,
            List<string> foundNonTerminals, List<string> terminals)
        {
            return (RuleContainsNonTerminal(rule, foundNonTerminals, otherNonTerminals)
                && RuleContainsTerminal(rule, terminals))
                || RuleOutputConsistOfElementSet(rule, foundNonTerminals) || RuleOutputConsistOfElementSet(rule, terminals);
        }

        public static bool RuleIsValidToRemove(this GrammarRule rule, List<string> foundNonTerminals, List<string> terminals)
            => !(RuleOutputConsistOfElementSet(rule, foundNonTerminals) || RuleOutputConsistOfElementSet(rule, terminals));

        public static bool RuleOutputConsistOfElementSet(this GrammarRule rule, List<string> elements)
        {
            if (elements.Count == 0)
                return false;
            GrammarRule ruleCopy = new GrammarRule(rule.NonTerminal, rule.Output);
            foreach (var item in elements)
                ruleCopy.ChangeOuptut(ruleCopy.Output.Replace(item, string.Empty));

            return string.IsNullOrEmpty(ruleCopy.Output.Trim());
        }

        public static bool RuleContainsTerminal(this GrammarRule rule, List<string> terminals)
        {
            foreach (var terminal in terminals)
            {
                if (rule.Output.Contains(terminal))
                    return true;
            }
            return false;
        }

        public static bool RuleContainsNonTerminal(this GrammarRule rule, List<string> foundNonTerminals, IEnumerable<string> otherNonTerminals)
        {
            if (foundNonTerminals.Count == 0)
            {
                if (otherNonTerminals.Any(symbol => rule.Output.Contains(symbol)))
                    return false;
                return true;
            }
            foreach (var nonTerminal in foundNonTerminals)
            {
                if (rule.Output.Contains(nonTerminal))
                    return true;
            }
            return false;
        }
    }
}
