﻿using System.Collections.Generic;
using System.Linq;
using Entities;

namespace GrammarParserLib
{
    internal class RemoveBarrenSymbolsAlg
    {
        public ResultOperation RemoveBarrenSymbols(Grammar grammar)
        {
            List<string> N = CalculateN(grammar);
            if (!N.Contains(grammar.Axiom))
                return ResultOperation.LanguageIsEmpty;
            RemoveBarrenSymbols(grammar, N);
            return ResultOperation.AlgSuccess;
        }

        private List<string> CalculateN(Grammar grammar)
        {
            List<string> N = new List<string>();
            List<string> newN = new List<string>();
            List<string> previousNewN = new List<string>();
            do
            {
                newN.Clear();

                grammar.NonTerminals
                .ForEach(nonterminal => newN
                .AddRange(grammar.Rules
                .FindAll(rule => rule.NonTerminal == nonterminal && rule
                .RuleOuputConsistsOfFoundNonterminalsAndTerminals(grammar.NonTerminals
                .Except(N), N, grammar.Terminals))
                .Select(rule => rule.NonTerminal)
                ));

                newN = newN
                .Distinct()
                .ToList();

                if (!GrammarAlgorithms.CompareLists(newN, previousNewN))
                {
                    N.AddRange(newN);
                    previousNewN = new List<string>(newN);
                }
                else
                    break;
            } while (newN.Count != 0);
            return N;
        }

        private void RemoveBarrenSymbols(Grammar grammar, List<string> N)
        {
            var rulesToDelete = grammar.Rules
            .FindAll(rule => rule
            .RuleIsValidToRemove(N, grammar.Terminals));

            grammar.RemoveRules(rulesToDelete);

            grammar.RemoveNonTerminals(grammar.NonTerminals
            .FindAll(nonTerminal => !N
            .Contains(nonTerminal)));

            grammar
            .RemoveTerminals(grammar.Terminals
            .FindAll(terminal => !grammar.Rules
            .Any(rule => rule.Output
            .Contains(terminal))));
        }

    }
}
