﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entities;

namespace GrammarParserLib
{
    internal class RemoveUnreachableSymbolsAlg
    {
        public ResultOperation StartAlgorithm(Grammar grammar)
        {
            RemoveUnreachableSymbols(grammar, CalculateW(grammar));
            return ResultOperation.AlgSuccess;
        }

        private List<string> CalculateW(Grammar grammar)
        {
            List<string> W = new List<string>(new[] { grammar.Axiom });
            List<string> newW = new List<string>();
            List<string> previousNewW = new List<string>();
            do
            {
                newW.Clear();
                foreach (var item in W)
                {
                    if (grammar.NonTerminals.Contains(item))
                    {
                        newW
                        .AddRange(grammar.Rules
                        .FindAll(rule => rule.NonTerminal == item)
                        .Select(rule => SelectUniqueSymbol(rule, W, grammar.Terminals, grammar.NonTerminals)));
                    }
                }
                newW = newW.Distinct().ToList();
                newW.RemoveAll(str => str == null);
                if (!GrammarAlgorithms.CompareLists(newW, previousNewW))
                {
                    W.AddRange(newW);
                    previousNewW = new List<string>(newW);
                }
                else
                    break;
            } while (newW.Count != 0);
            return W;
        }

        private string SelectUniqueSymbol(GrammarRule rule, IEnumerable<string> reachableSymbols, List<string> terminals, List<string> nonTerminals)
        {
            foreach (var item in nonTerminals)
            {
                if (reachableSymbols.Contains(item))
                    continue;
                if (rule.Output.Contains(item))
                    return item;
            }
            foreach (var item in terminals)
            {
                if (reachableSymbols.Contains(item))
                    continue;
                if (rule.Output.Contains(item))
                    return item;
            }
            return null;
        }

        private void RemoveUnreachableSymbols(Grammar grammar, List<string> w)
        {
            var newNonTerminals = grammar.NonTerminals
            .Intersect(w)
            .ToList();

            var newTerminals = grammar.Terminals
            .Intersect(w)
            .ToList();

            var newRules = grammar.Rules
            .FindAll(rule => newNonTerminals
            .Contains(rule.NonTerminal) && rule
            .RuleOuputConsistsOfFoundNonterminalsAndTerminals(grammar.NonTerminals
            .Except(w), newNonTerminals, newTerminals));

            grammar.RemoveRules(grammar.Rules
            .Except(newRules));

            grammar.RemoveTerminals(grammar.Terminals
            .Except(newTerminals));
            grammar.RemoveNonTerminals(grammar.NonTerminals
            .Except(newNonTerminals));
        }
    }
}
